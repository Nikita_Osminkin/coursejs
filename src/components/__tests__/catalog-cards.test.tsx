import React from 'react';
import { mount } from 'enzyme';
import CatalogCards from '../catalog-cards';
import { StaticRouter } from 'react-router-dom';
import { describe, it, expect } from '@jest/globals';
import { ComputerFor1Card } from '../../assets/import';

describe('Тестирование CatalogCards', () => {

    it('Тестируем рендер CatalogCards', async () => {
        const component = mount(
            <StaticRouter>
                <CatalogCards id={5} loading name="Computer" image={ComputerFor1Card} price={50000} processor motherboard videocard RAMmemory hdd ssd powerSupplyUnit onClickAddProducts addedCount />
            </StaticRouter>
        )

        expect(component).toMatchSnapshot()
    });
});