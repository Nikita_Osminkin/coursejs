const router = require("express").Router();

router.get('/catalog', (req, res) => {
    res.send(require('./json/products/sucsess.json'));
});

module.exports = router;